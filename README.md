# Person Follower for Mobile Robot

## Disclosure

This is a final year BSc exploratory software project carried out at the University of Leeds, you are allowed to use this project where appropriate accreditation has been given, by referencing the original author and the institution as follows:

## This project is built upon project made by Ilyass Taouil - Human_Position_Estimation([Found here](https://github.com/itaouil/human_position_estimation)) 
**LaTeX Reference:**
```latex
@misc{itaouil18fyp,
  author         = {Ilyass Taouil},
  title          = {{3D Human Position Estimation}},
  year           = {2018},
  url            = {{https://github.com/itaouil/human_position_estimation.git}},
  note           = {School of Computing, The University of Leeds}
}
```
## For use of this Person Follower project
**LaTeX Reference:**
```latex
@misc{sc16dcf,
  author         = {Daniel Fryer},
  title          = {{Person Follower for Mobile Robot}},
  year           = {2019},
  url            = {{https://gitlab.com/sc16dcf/ml1_person_follower.git}},
  note           = {School of Computing, The University of Leeds}
}
```

## Introduction

This repository hosts my final year project, which is a **ROS package** that Recognises someone to an okay level. The Recognition is base don three features:

1. Colour histograms
2. Face recognition
3. Location using a predictive state model (Kalmn Filter)


The package is very large and may need a powerful computer in order to run as desired. it is somewhat efficient for a project which does so much processing.

## What Do I Need To Run The Project?

The project does not have many software dependencies as it only relies mostly on the ROS middleware and the OpenCV library(aswell as others in the wiki). Moreover, this project assumes that you have access to TIAGo or a simulated TIAGo

## How Can I Run It?

All information regarding the installation process can be found in the [Wiki](https://gitlab.com/sc16dcf/ml1_person_follower/wikis/Installing-and-Running-the-project). 



