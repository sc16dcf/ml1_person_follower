"""
    The following script initialises the chest area picture to compare
    ther colour histograms in colour_probabilitys
"""

# Modules
import os
import cv2
import sys
# import rospy
import math
import message_filters
import numpy as np
from numpy.linalg import inv
from matplotlib import pyplot as plt
from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray
from human_position_estimation.srv import *
from human_position_estimation.msg import *

class Kalman_Probability:

    def __init__(self):
        self.x_observations = np.array([])
        self.y_observations = np.array([])
        # Initial Conditions
        self.time = 1 # Difference in time
        # Process / Estimation Errors
        self.error_estimation_x = 3 #
        self.error_estimation_v = 0.4 # error estimation
        # Observation Errors
        self.error_observation_x = 0.2 # Uncertainty in the measurement x
        self.error_observation_v = 0.2  #uncertainty in v
        self.P = self.kalmanCovariance(self.error_estimation_x, self.error_estimation_v)
        self.A = np.array([[1, self.time],
                      [0, 1]])


    def kalmanPredict(self,x, v, t, a):
        A = np.array([[1, t],
                      [0, 1]])
        X = np.array([[x],
                      [v]])
        B = np.array([[0.5 * t ** 2],
                      [t]])
        X_prime = A.dot(X) + B.dot(a)
        return X_prime

    def kalmanCovariance(self,sigma1, sigma2):
        cov1_2 = sigma1 * sigma2
        cov2_1 = sigma2 * sigma1
        cov_matrix = np.array([[sigma1 ** 2, cov1_2],
                           [cov2_1, sigma2 ** 2]])
        return np.diag(np.diag(cov_matrix))


    def update(self,xcoord1, xcoord2, xcoord3, xcoord4):
        self.v = xcoord2-xcoord1

        self.x_observations = np.array([xcoord1, xcoord2, xcoord3, xcoord4])
        vel = xcoord2 - xcoord1
        vel2 = xcoord3 - xcoord2
        vel3 = xcoord4 - xcoord3
        FinInitVel = vel2-vel
        self.acc = FinInitVel/1  # Acceleration
        self.y_observations = np.array([0,vel, vel2, vel3])
        self.z = np.c_[self.x_observations, self.y_observations]
        self.X = np.array([[self.z[0][0]],
                      [self.v]])
        self.n = len(self.z[0])

    def returnable(self):
        return self.returnableArray
    def initialiser(self):
        # try:
            for data in self.z[1:]:
                X = self.kalmanPredict(self.X[0][0], self.X[1][0], self.time, self.acc)
                # set off-diagonal terms to 0.
                P = np.diag(np.diag(self.A.dot(self.P).dot(self.A.T)))
                # Calculating kalman gain
                H = np.identity(self.n)
                R = self.kalmanCovariance(self.error_observation_x, self.error_observation_v)
                S = H.dot(P).dot(H.T) + R
                K = P.dot(H).dot(inv(S))
                # Reshape the new data
                Y = H.dot(data).reshape(self.n, -1)
                # Update the State Matrix
                X = X + K.dot(Y - H.dot(X))
                # Update Process Covariance Matrix
                P = (np.identity(len(K)) - K.dot(H)).dot(P)

            # print "Kalman Filter State Matrix:\n", X
            self.returnableArray = X
        # except Exception as e:
        #     print("Something is wrong, ", e)
def main(xcoord1, xcoord2, xcoord3, xcoord4):

    # Initialise node
    # rospy.init_node('subscriptions_sync', anonymous=True)
    try:
        # Subscriptions (via Subscriber package)
        kp = Kalman_Probability()
        # poses_sub  = rospy.Subscriber("/detections", Detections, kp.intialiser)
        kp.update(xcoord1, xcoord2, xcoord3, xcoord4)
        kp.initialiser()
        data = kp.returnable()
        return data
        # Spin it baby !
        # rospy.spin()
        # rospy.on_shutdown(finish)
    except KeyboardInterrupt as e:
        print('Error during main execution' + e)

# Execute main
if __name__ == '__main__':
    main()
