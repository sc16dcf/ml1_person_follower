#!/usr/bin/env python
from __future__ import division
import cv2
import numpy as np
import sys
import roslib
import rospy
import tf
import math
import time
import actionlib
from human_position_estimation.srv import *
from human_position_estimation.msg import *
from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray
from move_base_msgs.msg import *
from actionlib_msgs.msg import *
from geometry_msgs.msg import Twist, Vector3, Pose, Point, Quaternion
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError

class Position():

    def __init__(self):
        self.sub = rospy.Subscriber('/poses', Poses, self.Callback)
        # self.sub = rospy.Subscriber('/detections', Detections, self.Callback)

    def Callback(self, data):
        self.goal_sent = False
    	# Tell the action client that we want to spin a thread by default
    	self.move_base = actionlib.SimpleActionClient("move_base", MoveBaseAction)
    	rospy.loginfo("Wait for the action server to come up")

    	# Allow up to 5 seconds for the action server to come up
    	self.move_base.wait_for_server(rospy.Duration(5))
        try:
            theta = 0
            position = {'x': data.array[0].pose.x, 'y' : data.array[0].pose.y}
            quaternion = {'r1' : 0.000, 'r2' : 0.000, 'r3' : np.sin(theta/2.0), 'r4' : np.cos(theta/2.0)}

            rospy.loginfo("Go to (%s, %s) pose", position['x'], position['y'])
            success = self.goto(position, quaternion)

            if success:
                rospy.loginfo("Hooray, reached the desired pose")
            else:
                rospy.loginfo("The base failed to reach the desired pose")

            # Sleep to give the last log messages time to be sent
            rospy.sleep(1)
        except rospy.ROSInterruptException:
            rospy.loginfo("Ctrl-C caught. Quitting")


    def goto(self, pos, quat):
            # Send a goal
        print("entered go to")
        self.goal_sent = True
        goal = MoveBaseGoal()
        goal.target_pose.header.frame_id = 'map'
        goal.target_pose.header.stamp = rospy.Time.now()
        goal.target_pose.pose = Pose(Point(pos['x'], pos['y'], 0.000), Quaternion(quat['r1'], quat['r2'], quat['r3'], quat['r4']))

    # Start moving
        self.move_base.send_goal(goal)

    # Allow TurtleBot up to 60 seconds to complete task
        success = self.move_base.wait_for_result(rospy.Duration(60))

        state = self.move_base.get_state()
        result = False

        if success and state == GoalStatus.SUCCEEDED:
            # We made it!
            result = True
        else:
            self.move_base.cancel_goal()

        self.goal_sent = False
        return result

        # pub = rospy.Publisher('mobile_base/commands/velocity', Twist, queue_size = 10)
        # rate = rospy.Rate(10) #10hz
        # desired_velocity = Twist()
        # desired_velocity.linear.x = 0.2 # Forward with 0.2 m/sec.
        # while not rospy.is_shutdown():
        #     pub.publish(desired_velocity)

def main(args):
	# Instantiate your class
	# And rospy.init the entire node
	rospy.init_node('position_robot', anonymous=True)
	cI = Position()
	rospy.spin()
	# Ensure that the node continues running with rospy.spin()
	# You may need to wrap rospy.spin() in an exception handler in case of KeyboardInterrupts

	# Remember to destroy all image windows before closing node

if __name__ == '__main__':
    	main(sys.argv)
