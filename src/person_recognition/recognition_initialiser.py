# !/usr/bin/env python

"""
    The following script initialises the chest area picture to compare
    ther colour histograms in colour_probabilitys
"""

# Modules
import os
import cv2
import sys
import rospy
import math
import time
import message_filters
import numpy as np
from matplotlib import pyplot as plt
from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray
from human_position_estimation.srv import *
from human_position_estimation.msg import *
# Path library
from pathlib import Path

# OpenCV-ROS bridge
from cv_bridge import CvBridge, CvBridgeError

# Messages for requests and subscriptions
from sensor_msgs.msg import Image, PointCloud2
from human_position_estimation.srv import *
PATH = str(Path(os.path.dirname(os.path.abspath(__file__))).parents[0])
class recognition_initialiser:
        # Constant path
        def __init__(self):
            self.PATH = str(Path(os.path.dirname(os.path.abspath(__file__))).parents[0])
            self.x = 0
            self.y = 0
            self.z=[0,0]
            self.counter = 0
            self.n = 0
            self.i = 0
            self.img = cv2.imread(self.PATH + "/data/converted/recognition_image.png")
        def finish(self):
            print "captured image successfully"

        def processFirstDetection(self, i):
            print("Got xy coords")
            # Processing the rgb image
            print("storing cropped image of chest")
            Chest_detect_image = self.crop(self.x,self.y)
            self.store_image(Chest_detect_image)
            print("processing face", i)
            face = self.cropFace(self.x,self.y)
            self.store_face(face, str(i))
            i = i+1
            time.sleep(1)


        def crop(self,x,y):
            self.img = cv2.imread(self.PATH + "/data/converted/recognition_image.png")
            crop_img = self.img[y-100:y, x-50:x+50]
            return crop_img

        def cropFace(self,x,y):
            self.img = cv2.imread(self.PATH + "/data/converted/recognition_image.png")
            crop_img = self.img[y-220:y-50, x-60:x+60]
            return crop_img

        def store_face(self,face, i):
            cv2.imwrite(self.PATH + "/person_recognition/faces/faceDataset/Person_ToFollow/face"+i+".png", face)


        def store_image(self,Chest_detect_image):
            cv2.imwrite(self.PATH + "/person_recognition/histograms/initialPersonToFollow.png", Chest_detect_image)

        def updater(self,data):
            try:
                print("updating coords of person")
                self.x = data.array[0].centre_x
                self.y = data.array[0].centre_y
                # print("updating coords of person", self.x, self.y)
            except KeyboardInterrupt as e:
                print('Error during update' + e)
        def toMAT(self,rgb_image):
            """
                Converts raw RGB image
                into OpenCV's MAT format.

                Arguments:
                    sensor_msgs/Image: RGB raw image

                Returns:
                    MAT: OpenCV BGR8 MAT format
            """
            try:
                cv_image = CvBridge().imgmsg_to_cv2(rgb_image, 'bgr8')
                return cv_image

            except Exception as CvBridgeError:
                print('Error during image conversion: ', CvBridgeError)


        def store(self,cv_image):
            """
                Stores the converted raw image from
                the subscription and writes it to
                memory.

                Arguments:
                    MAT: OpenCV formatted image
            """
            print("*******Updating Stored Image ***")
            cv2.imwrite(PATH + "/data/converted/recognition_image.png", cv_image)

        def processSubscriptions(self,rgb_image, depth_image):
            """
                Callback for the TimeSynchronizer
                that receives both the rgb raw image
                and the depth image, respectively
                running the detection module on the
                former and the mappin process on the
                former at a later stage in the chain.

                Arguments:
                    sensor_msgs/Image: The RGB raw image
                    sensor_msgs/Image: The depth image
            """
            print("Got depth and rgb.")
            # Processing the rgb image
            rgb_cv_image = self.toMAT(rgb_image)
            self.store(rgb_cv_image)
            if self.n < 1:
                detections_sub  = rospy.Subscriber("/detections", Detections, self.updater)
                self.n=self.n+1
                rospy.sleep(0.3)

            if self.i < 38:
                self.processFirstDetection(self.i)
                self.i = self.i+1
                if self.i == 4:
                    self.n=0
                if self.i == 10:
                    self.n=0
                if self.i == 14:
                    self.n=0
                if self.i == 20:
                    self.n=0
                if self.i == 24:
                    self.n=0


def main(args):

    # Initialise node
    rospy.init_node('recognition_initialiser', anonymous=True)
    try:
        ri = recognition_initialiser()
        # Subscriptions (via Subscriber package)
        rgb_sub   = message_filters.Subscriber("/xtion/rgb/image_rect_color", Image)
        depth_sub = message_filters.Subscriber("/xtion/depth_registered/image_raw", Image)


        # Synchronize subscriptions
        ats = message_filters.ApproximateTimeSynchronizer([rgb_sub, depth_sub], queue_size=5, slop=0.5)
        ats.registerCallback(ri.processSubscriptions)

        # ri.processFirstDetection
        # Spin it baby !
        rospy.spin()
        rospy.on_shutdown(ri.finish)
    except KeyboardInterrupt as e:
        print('Error during main execution' + e)

# Execute main
if __name__ == '__main__':
    main(sys.argv)
