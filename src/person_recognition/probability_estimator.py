
import cv2
import numpy as np
import sys
import roslib
import rospy
import tf
import math
import time
import actionlib
from human_position_estimation.srv import *
from human_position_estimation.msg import *
from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray
from move_base_msgs.msg import *
from actionlib_msgs.msg import *
from geometry_msgs.msg import Twist, Vector3, Pose, Point, Quaternion
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import recognition_initialiser
import colour_probability
import kalman_probability

class ProbabilityEstimator:

    def __init__(self):
        """
            Constructor.
        """


    def Estimate(self):
            #loop over all detections and compare all of them to the kalman prediction. the closest gets a higher score
            dataKalman = kalman_probability.main(1,2,3,4)
            print "The predicted x location", int(dataKalman[0])
            print "The predicted velocity", int(dataKalman[1])


            #find number of detections, call recognise_person_face.py with the number of the dection passed. in recognise_face we can use that to seach for an recognition_image. maker it return true or false. if it take slonger than 2 seconds abandone
            #i = 0
            #while i < number_of_detections:
            #    dataface = recognise_personface.main(i)
            #    if dataface == True:
            #       score array[i] == 1
            #       break;

            #process resulkts here and update new person to follow
            #
            #the array with highest score gets made perosn to follow. publish the pose of that person

def main(args):

    # Initialise node
    #rospy.init_node('probabilityEstimator', anonymous=True)

    try:
        # Initialise
        hd = ProbabilityEstimator()
        hd.Estimate()
        #rospy.spin()

    except KeyboardInterrupt as e:
        print('Error during main execution' + e)

# Execute main
if __name__ == '__main__':
    main(sys.argv)
