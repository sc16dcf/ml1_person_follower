# !/usr/bin/env python

"""
    The following script grabs the rgb image converts it to hsv
    and returns colour histogram intersections of each person in the frame
    tp the person we need to follow (done by recognition_initialiser)
"""

# Modules
import os
import cv2
import sys
import rospy
import math
import message_filters
import numpy as np
from matplotlib import pyplot as plt
from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray
from human_position_estimation.srv import *
from human_position_estimation.msg import *
# Path library
from pathlib import Path

# OpenCV-ROS bridge
from cv_bridge import CvBridge, CvBridgeError

# Messages for requests and subscriptions
from sensor_msgs.msg import Image, PointCloud2
from human_position_estimation.srv import *
class Colour_Probability:

    def __init__(self):
        self.PATH = str(Path(os.path.dirname(os.path.abspath(__file__))).parents[0])
        self.index = {}
        self.results = {}
        self.rate = rospy.Rate(5) # 10hz
        self.numberOfDet = 0
        self.colours = Colours()
        self.colour_pub = rospy.Publisher('colours', Colours, queue_size=5)

    def return_intersection(self):
        image = cv2.imread(self.PATH + "/person_recognition/histograms/initialPersonToFollow.png")
        histToComp = cv2.calcHist([image], [0, 1, 2], None, [8, 8, 8],[0, 256, 0, 256, 0, 256])
        histToComp = cv2.normalize(histToComp, histToComp).flatten()
        for (i, hist) in self.index.items():
            res = cv2.compareHist(histToComp, hist, cv2.HISTCMP_INTERSECT)
            colour = Colour()
            colour.ID = 'array['+i+']'
            colour.difference = res

            # Aggregate the detection to the others
            self.colours.array.append(colour)
            self.results['array['+i+']'] = res
        return self.results
    	# # sort the results
    	# results = sorted([(v, k) for (k, v) in results.items()], reverse = reverse)

    def initialiseHistogram(self,i):
        # print "making histogram for array["+i+"]"
        image = cv2.imread(self.PATH + "/person_recognition/histograms/OtherPeopleInView/array["+i+"].png")
        hist1 = cv2.calcHist([image], [0, 1, 2], None, [8, 8, 8],
    		[0, 256, 0, 256, 0, 256])
        hist1 = cv2.normalize(hist1, hist1).flatten()
        self.index[i] = hist1

    def finish(self):
        print "histograms compared sucessfully"

    def processDetection(self,x,y, i):
        # print("Got xy coords, processing image for array["+i+"]")
        # Processing the rgb image
        Chest_detect_image = self.crop(x,y)
        self.store_image(Chest_detect_image, i)

    def crop(self,x,y):
        img = cv2.imread(self.PATH + "/data/converted/image.png")
        crop_img = img[y-100:y, x-50:x+50]
        return crop_img

    def store_image(self,Chest_detect_image, i):
        cv2.imwrite(self.PATH + "/person_recognition/histograms/OtherPeopleInView/array["+i+"].png", Chest_detect_image)


    def starter(self,data):
        try:
            i=0
            self.numberOfDet = data.number_of_detections
            print "number of detections:", self.numberOfDet
            while i < int(self.numberOfDet):
                x = data.array[i].centre_x
                y = data.array[i].centre_y
                self.processDetection(x,y,str(i))
                i = i + 1
                rospy.sleep(0.03)
            k=0
            if i >= int(self.numberOfDet):
                while k < int(self.numberOfDet):
                    self.initialiseHistogram(str(k))
                    k=k+1
                    rospy.sleep(0.03)
            probability = self.return_intersection()
            self.colour_pub.publish(self.colours)
            #clean up
            self.colours = Colours()
            self.index = {}
            self.results = {}
            self.numberOfDet = 0
            # pub_results.publish(probability)
            # print "\n results, the higher the number the more like the perosn in array is to person we need to follow"
            print probability, "\n"
            rospy.sleep(0.01)
            reason = "our purpose is complete, we have intiialised the colour of the first person seen"
        except Exception as e:
            print("Something is wrong, ", e)
def main(args):

    # Initialise node
    rospy.init_node('subscriptions_sync', anonymous=True)

    try:
        # Subscriptions (via Subscriber package)
        cp = Colour_Probability()
        detections_sub  = rospy.Subscriber("/detections", Detections, cp.starter)

        # Spin it baby !
        rospy.spin()
        rospy.on_shutdown(finish)
    except KeyboardInterrupt as e:
        print('Error during main execution' + e)

# Execute main
if __name__ == '__main__':
    main(sys.argv)
